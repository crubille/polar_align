Polar Align is a software to help align an equatorial mount using sky images fram a camera.

# Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, 
CS 60 319, 60203 Compiègne Cedex France


This is the source distribution of c_polar_align for linux.
It require standart X11 includes and g++ plus:
   - gphoto2
   - libjpeg

The distribution include CImg, Ephemeris and GetPot.

The star detection algorithm is from https://github.com/carsten0x51h/star_recognizer_code_snippets. So part of the files ar
e under "Copyright (C) 2015 Carsten Schmitt".

Stars catalogs are from VizieR Astronomical Server vizier.u-strasbg.fr.

