/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2015 Carsten Schmitt
 * Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compiègne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

typedef tuple<int /*x*/,int /*y*/> PixelPosT;
typedef set<PixelPosT> PixelPosSetT;
typedef list<PixelPosT> PixelPosListT;
 
typedef tuple<float, float> PixSubPosT;
typedef tuple<float /*x1*/, float /*y1*/, float /*x2*/, float /*y2*/> FrameT;
 
struct StarInfoT {
  FrameT clusterFrame;
  FrameT cogFrame;
  FrameT hfdFrame;
  PixSubPosT cogCentroid;
  PixSubPosT subPixelInterpCentroid;
  float hfd;
  float maxPixValue;

  float rotation; // computed as vectorial product of the center to segment extremity divede by radius
  float quality; // estimated as the abs of the vectorial product of the 2 segments
};
typedef list<StarInfoT> StarInfoListT;
 

struct SegmentT {
  PixSubPosT begin_pos;
  float begin_hfd;
  float begin_maxPixValue;
  PixSubPosT end_pos;
  float end_hfd;
  float end_maxPixValue;
};
typedef list<SegmentT> SegmentListT;
 

bool compare_stars( StarInfoT s_a, StarInfoT s_b)
{
  return s_a.maxPixValue * s_a.hfd > s_b.maxPixValue * s_b.hfd;
}
bool compare_intersections( StarInfoT s_a, StarInfoT s_b)
{ // order for intersections lists
  return s_a.rotation > s_b.rotation;
}
bool assez_proche( StarInfoT s_a, StarInfoT s_b)
{ // check close rotation
  if( s_a.rotation > 0 )
    return  s_a.rotation  < 1.05* s_b.rotation && s_b.rotation  < 1.05 * s_a.rotation;
  else return  s_a.rotation  > 1.05* s_b.rotation && s_b.rotation  > 1.05 * s_a.rotation;;
}

bool too_long( StarInfoT Center, StarInfoT inter ) 
{ // used to found abberant intersection
  float diff;
  PixSubPosT CenterCentroid = Center.cogCentroid;
  PixSubPosT interCentroid =  inter.cogCentroid;
  diff = fabs( std::get<0>(CenterCentroid) - std::get<0>(interCentroid) );
  if( diff > 15. ) return true;
  diff = fabs( std::get<1>(CenterCentroid) - std::get<1>(interCentroid) );
  if( diff > 15. ) return true;
  return false;
}
 

/**
* Get all pixels inside a radius: http://stackoverflow.com/questions/14487322/get-all-pixel-array-inside-circle
* Algorithm: http://en.wikipedia.org/wiki/Midpoint_circle_algorithm
*/
bool
insideCircle(float inX /*pos of x*/, float inY /*pos of y*/, float inCenterX, float inCenterY, float inRadius)
{
  return (pow(inX - inCenterX, 2.0) + pow(inY - inCenterY, 2.0) <= pow(inRadius, 2.0));
}

float distance2( float x1, float y1, float x2, float y2)
{
  return ( pow(x1-x2 , 2.0) + pow( y1-y2 , 2.0));
}

float x_vector( float u1, float u2, float v1, float v2)
{
  return (u1*v2) - (u2*v1);
}


void print_list_stars( const string img_name, StarInfoListT *starInfos )
{
  // Output text info
  cout << "\t" << img_name << "=(" << endl; 
  for (StarInfoListT::iterator it = starInfos->begin(); it != starInfos->end(); ++it) {
    StarInfoT * curStarInfo = & (*it);
    PixSubPosT & cogCentroid = curStarInfo->cogCentroid;
    float & hfd = curStarInfo->hfd;
    float & maxPixValue = curStarInfo->maxPixValue;
    
    cout << (curStarInfo->rotation) << "   \t" 
	 <<  ",\t" << std::get<0>(curStarInfo->cogCentroid)
	 << ",\t " << std::get<1>(curStarInfo->cogCentroid)
	 << "\t/\t" <<(curStarInfo->quality) 
	 << endl;
    
  }
  cout << ")";
  return  ;
}

bool InZoom( float x, float y )
{ 
  if( fabsf(MES_preferences.zoom_x/MES_preferences.Win_Scale -x)
      > MES_Display->Win_Width/2. ) return false;
  if( fabsf(MES_preferences.zoom_y/MES_preferences.Win_Scale-y)
      > MES_Display->Win_Height/2. ) return false;
  return true;
}
bool InImage( float x, float y, CImg<unsigned char> *img, float x_c, float y_c)
{
  if(MES_preferences.zoom
     && InZoom(x_c, y_c)
     )
    {
      return InZoom(x,y );
    }
  else {
    if( (x <= 0) || (y <= 0)) return false;
    if( (x >= img->width()) || (y >= img->height())) return false;
  }
  return true;
}
