/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2020 Paul Crubill� Sorbonne universit�s, Universit� de Technologie de Compi�gne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compi�gne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using namespace std;
#include <time.h>
#include <string>
#include <iostream>
#include <string>
#include <stdlib.h>
#include "lang.h"
#include "../getpot-c++/GetPot.hpp"
#include "preferences.h"

///////////////////////////////////////////////////////////////////////////
//           This file is a prototype for anybody who want to adapt
//           the software to a new language.
//           Due to Cimg, it use ISO_8859-1 character set -
//           elsewhere you should load a new font in cimg.

// just translate each message and test it adding the "-user_language" option.
// to integrate it in a better way, recopy this file as a new .cpp one,
// change the type and object name, cmplete the Makefile.
// To add a specific option, just add the entry in preferences.h and
// an extern declaration for an object of the base type Lang_DisplayT in lang.h

///////////////////////////////////////////////////////////////////////////
class User_Language_DisplayT : public Lang_DisplayT
{
 public:
  string help()
    {
      return "Help" ;
    }

  string windows_title()
    {
      return "C polar align" ;
    }

  string info_0()
  {
    stringstream s0;
    s0 <<  "Help to align an equatorial mount using 2 images of the polar region of the sky."
      << endl
      << "Between the 2 images, the RA axis of the mount should be rotated."
       <<endl
       <<  "Then the software overprint the nominal position of stars around the computed" << endl
      << "rotation center and periodically get new images from the camera. " <<endl
       << "Then use the alt/az adjustment off the mount to bring the stars to their nominal position "
      << endl << endl
       << "Software options can be set in a config file or on the command line.";
    return s0.str();
  }
    
  string info_1()
  {
    return  "User language - Step 1:";
  }
  string info_1_1()
  {
stringstream s1;
    s1 <<  "1-1 - Align your mount to the pole using a compass for example" <<endl
       <<  "and adjust it for the lattitude.";
    return s1.str();
  }
  string info_1_2()
  {
    stringstream s2;
    s2 << "1-2 - Using the DEC adjustment set the camera to point to the polar region"
       << endl << 		       "(do not modify the DEC adjustment for the rest of the process).";
    return s2.str();
  }
  string info_1_3()
  {
    stringstream s3;
    s3 << "1-3 - Set the RA adjustment so that the camera image is tilted between 30 and 90 degrees" << endl
       <<  "(check the lens focus is good enough to see stars)";
    return s3.str();
  }
  string info_1_4()
  {
    stringstream st;
    st << "1-4 press \"y\", \"c\", <esc>, <space>, <cr> to process: get an image from the camera and go to step2," << endl 
       << "\"r\" to restart, \"q\" to quit.";
    return st.str();
  }
  string info_1_status()
  {
    stringstream ss;
    if( MES_preferences.Pole == NorthPole ) ss << "North pole" << endl;
      else ss << "South pole" << endl;
    if( MES_preferences.Lens == PhotoLens ) ss << "camera lens" << endl;
      else ss << "astro lens" << endl;
    ss << "longitude = " << MES_preferences.longitude << endl
      << "images prefix = " << MES_preferences.JpegName << endl
      << "pixel size = " << MES_preferences.PixelSize << endl
      << "focale = " << MES_preferences.Focale << endl ;
    if(MES_preferences.ra_motor_on) ss << "RA motor on";
    else ss << "ra motor off"; 
    return ss.str();
  }
  
  string info2() // print message after i get and display the first image
  {
    stringstream s1;
    s1 <<  "If you are not ok with this image" << endl
      << "check focus, iso and speed and" << endl
       << "type \"r\" to get another one.";
    return s1.str();
  }
  string info_2_0()
  {
    return "Step 2:";
  }
  string info_2_1()
  {
    stringstream s2;
    s2 << "2-1 Else in order to continue" << endl
       << "set the RA adjustment " << endl
       << "so that the camera image " <<endl
       << "is close to horizontal.";
    return s2.str();
  }
  string info_2_2()
  {
    stringstream st;
    st << "2-2  press \"y\", \"c\", <esc>, <space>, <cr> to process" << endl
       << "\"r\" to restart, \"q\" to quit, "
      ;
    return st.str();
  }

  string info_3() // print message after i get and display the first image
  {
    return "C Polar Align";
  }
  string info_3_00()
  {
    return "Rotation center detected.";
  }
  string info_3_0()
  {
    return "Step 3:";
  }
  string info_3_1()
  {
    stringstream s1;
    s1    << "Now, use the alt/az ajustment to put each" << endl
	  << "star at its position (in the yellow circle)."
	  << endl << "If needed, you can rotate the sight (to"
	  << endl << "adapt to imprecise camera position,"
	  << endl << "longitude or time synchronisation).";

    return s1.str();
  }
  string info_3_2()
  {
    stringstream st;
    st << "3-2  press \"y\", \"c\", <esc>, <space>, <cr> to get a new image" << endl
       << "\"r\" to restart, \"q\" to quit, " << endl
       << "\"z\", <left clic> to zoom on the mouse pointer position;" <<endl
       << "\"u\", <right clic> to unzoom (<esc> unzoom too)."<<endl
       << "\"+\"/\"-\" (from the numeric pad) to rotate stars position.";
    return st.str();
  }
 
  string info_get_image()
  {
    return "asking camera for an image";
  }

  string info_get_stars()
  {
    return "computing stars";
  }

 string info_get_center()
  {
    return "computing rotation center";
  }

 string  msg_info4_few_stars() // print message 
  {
    return " stars found. Centre detection will not be very good";
  }

 string msg_info4_unable() // print message 
  {
    return " Unable to load image " ;
  }

  virtual string msg_info4_no_center() // print message 
  {
    return  "Rotation center not found";
  }

 string msg_info4_no_star() // print message 
  {
    return  "Not enough stars found " ;
  }
};

Lang_DisplayT *User_Language = new User_Language_DisplayT;

