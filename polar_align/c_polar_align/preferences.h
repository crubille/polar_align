/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compiègne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#define NorthPole -1
#define SouthPole 1
#define AstroLens -1
#define PhotoLens 1

const string DEFAULT_config_file = "/config.cpa";

class MES_preferencesT
{
 public:
  Lang_DisplayT *Lang ; // used Language
  int Pole ; //  NorthPole or SouthPole
  int Lens ; // PhotoLens or AstroLens
  float longitude ;
  int Screen_Image_Width ;  float Win_Scale ;
  float Focale ;
  float PixelSize ;
  int Max_Print_Cat_Stars ; // 12 is enough
  string JpegName ;
  float rotate ;
  struct tm *gmt_now ;  // If AD motor is on, should not be updated...
  time_t l_now;
  bool ra_motor_on ;
  string GetPhoto ;
  int font_size;
  bool fullscreen ;
  bool zoom; float zoom_x, zoom_y;
  int wait_millisec ;
  private:
    char bufchar[80];
    char bufchar1[80];
 public:
  //  unsigned long long stars_in_img(tuple<float /*right_asc/,float /*decl*/> center,
  //			     tuple<int /*x*/,int /*y*/> sizexy,
  //  float scale /*arc_sec per pixel*/)
  MES_preferencesT(){
    update_time(true);
    Pole = NorthPole ; 
    Lens = PhotoLens ; 
    longitude = 0;
    Screen_Image_Width = 800;  Win_Scale = 1;
    Focale = 55.;
    PixelSize = 4.24;
    Max_Print_Cat_Stars = 24; // 24 is enough
    JpegName = "PA";
    rotate = 0;
    ra_motor_on = true;
    GetPhoto = "gphoto2  --force-overwrite --capture-image-and-download --filename ";
    font_size=20;
    fullscreen =false ;
    zoom = false; 
    wait_millisec = 5000;
    Lang = &En;
  }
  
  void cl_init( GetPot *cl ){
    if(cl->search(4, "-South", "-south","-SouthPole", "-southpole")) Pole = SouthPole;
    if(cl->search(4, "-Astro", "-astro","-AstroLens", "-astrolens")) Lens = AstroLens;

    if(cl->search("-longitude")) longitude = cl->next( 2.83); //default Compiègne
    if(cl->search("-screen_width"))
      {
	fullscreen =false ;
	Screen_Image_Width = cl->next(0) ;
      }
    if( cl->search("-focale")) Focale = cl->next( 135.); // mm
    if(cl->search("-pixelsize")) PixelSize = cl->next( 4.24 ); // micrometer alpha 5000
    if(cl->search("-stars_to_print")) Max_Print_Cat_Stars = cl->next( 10 ); //
    if(cl->search("-jpgname"))
      {
	JpegName = cl->next( "A" ); 
      };
    if( cl->search("-getphoto"))
      {
	GetPhoto =  cl->next( "gphoto2" );
      };
    if( cl->search("-ac_motor"))
      {
	ra_motor_on = true;
      };
    if( cl->search("-no_ac_motor"))
      {
	ra_motor_on = false;
      };
    if( cl->search("-font")) font_size = cl->next( 20); 
    if( cl->search("-fullscreen")) fullscreen = true; 
    if( cl->search("-fr"))
      {
	Lang = Fr;
      };
    if( cl->search("-short-text"))
      {
	Lang = Short_Text;
      };
    if( cl->search("-user_language"))
      {
	Lang = User_Language;
      };
    if(( cl->search("-help"))|| ( cl->search("-h")))
      {
	cerr << Lang->help();
      };
  }
  
  void update_time(bool force) // if force is false, look at ra_motor_on
  {
    unsigned long long now;
    //    struct timeval tv; struct timezone tz;
    //    gettimeofday( &tv, &tz);
    if( force || !ra_motor_on )
      {
	time( &l_now );
	gmt_now = (gmtime(&l_now));
	gmt_now->tm_mon+=1;
	gmt_now->tm_year+=1900;
      }
  }
  float champ_pixel_degrees()
  {
    return  (205./3600.)*PixelSize/Focale ;
  }
};


extern MES_preferencesT MES_preferences;


const unsigned char red[3] = {255,80,40}, green[3] = {0,255,0}, yellow[3] = {255,220,0};
const unsigned char black[3] = {0,0,0}, blue[3] = {0,0,255}, white[3] = {255,255,255};

