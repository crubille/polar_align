/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compiègne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Lang_DisplayT
{
 public:
  virtual string help();
  virtual string window_title();
  virtual string info_0();
  virtual string info_1();
  virtual string info_1_1();
  virtual string info_1_2();
  virtual string info_1_3();
  virtual string info_1_4();
  virtual string info_1_status();
  
  virtual string info2(); // print message after i get and display the first image
  virtual string info_2_0();
  virtual string info_2_1();
  virtual string info_2_2();
  
  virtual string info_3() ;// print message after i get and display the first image
  virtual string info_3_00();
  virtual string info_3_0();
  virtual string info_3_1();
  virtual string info_3_2();
  
  virtual string info_get_image();
  virtual string info_get_stars();
  virtual string info_get_center();
  virtual string  msg_info4_few_stars() ;// print message 
  virtual string msg_info4_unable() ;// print message 
  virtual string msg_info4_no_center() ;// print message 
  virtual string msg_info4_no_star() ;// print message 
};

extern Lang_DisplayT En;
extern Lang_DisplayT *Short_Text;
extern Lang_DisplayT *Fr;
extern Lang_DisplayT *User_Language;
