/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2015 Carsten Schmitt
 * Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compiègne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef VERY_SMALL_COMPUTER
#define  VERY_SMALL_COMPUTER_resize( IMG ) { ;} // if(IMG->width()>4000)IMG->resize_halfXY(); }
#else
#define  VERY_SMALL_COMPUTER_resize( IMG ) { ;}
#endif
/////////////
//#include <sys/time.h>
#include <time.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <assert.h>

#ifdef JPEG_LIB
#define cimg_use_jpeg
#endif
#include "../CImg/CImg.h"
#include "../getpot-c++/GetPot.cpp"

#include <tuple>
#include <functional>
#include <list>
#include <set>
#include <array>
#include <vector>

using namespace std;
using namespace cimg_library;

#define PI cimg_library::cimg::PI
#include "lang.h"
#include "preferences.h"
#include "display.h"
#include "stars.h"
#include "centre_rotation_monture.h"
#include "Catalogue.hpp"

int draw_img_liste_stars( CImg<unsigned char>* img, StarInfoT Center, CatStarListT *starInfos)
{
  int Win_Height;
  int Win_Width;
  float Win_Scale;
  size_t CrossSize;
  static CImg<unsigned char> rgbImg(img->width(), img->height(), 1 /*depth*/, 3 /*3 channels - RGB*/);
  // the goal is to alloc the big buffer statically(needed for zoom), 
  // and to avoid duplicate the code which write the virtual stars.
  // The pointer is set at the adress of the large buffer for zoom, and at the

  
  Win_Width  = MES_Display->win_width();
  Win_Scale = Win_Width; Win_Scale = Win_Scale / img->width();
  MES_preferences.Win_Scale = Win_Scale ;
  Win_Height = img->height() * Win_Scale;
  CImg<unsigned char> resized_rgbImg(Win_Width, Win_Height, 1, 3);
  CImg<unsigned char> *PrgbImg;
  if(MES_preferences.zoom)
    {
      Win_Scale = Win_Width; Win_Scale = Win_Scale / img->width();
      int C_x = MES_preferences.zoom_x/Win_Scale -Win_Width/2;
      int C_y = MES_preferences.zoom_y/Win_Scale -Win_Height/2 ;	
      if( C_x < 0) C_x = 0;       if( C_y < 0) C_y = 0;
      if( C_x > rgbImg.width() - Win_Width) C_x = rgbImg.width() - Win_Width;
      if( C_y > rgbImg.height() - Win_Height) C_y = rgbImg.height() - Win_Height;
      int x,y;
      for(x = C_x; x < C_x+Win_Width; x++)
	{
	  for( y = C_y; y < C_y+Win_Height; y++)
	    {
	      int value = (*img)(x,y);
	      rgbImg(x, y, 0 /*red*/) = value;
	      rgbImg(x, y, 1 /*green*/) = value;
	      rgbImg(x, y, 2 /*blue*/) = value;
	    }
	}
      Win_Scale = 1;
      PrgbImg = &rgbImg;
     }
  else {
    resized_rgbImg = img->get_resize(Win_Width,Win_Height, 1, 3);
    PrgbImg = &resized_rgbImg;
  }

  // Mark POLE cross
  float x_c = (std::get<0>(Center.cogCentroid)) *Win_Scale +0.5;
  float y_c = (std::get<1>(Center.cogCentroid)) *Win_Scale +0.5;
  CrossSize = 20. *Win_Scale;    
  PrgbImg->draw_line(floor( x_c -CrossSize), floor( y_c -CrossSize),
		   floor( x_c +CrossSize ), floor( y_c +CrossSize),
		   red, 1. /*opacity*/);
  PrgbImg->draw_line(floor( x_c -CrossSize), floor( y_c+CrossSize),
		   floor( x_c +CrossSize), floor( y_c-CrossSize),
		   red, 1. /*opacity*/);
  
  // Mark Max_Print_Cat_Stars from catalog in RGB image
  int nb_print_stars = 0;
  for (CatStarListT::iterator it = starInfos->begin();
       it != starInfos->end() && (nb_print_stars <= MES_preferences.Max_Print_Cat_Stars) ; ++it) {
    CatStarT * curStarInfo = & (*it);
    PixSubPosT Pos = Equ_Coord_to_pos_pixels( curStarInfo, Win_Scale );
    float R = Equ_Coord_to_r_pixels( curStarInfo->dec ) *Win_Scale;
    if( InImage( std::get<0>(Pos)+x_c, std::get<1>(Pos)+y_c, PrgbImg, x_c, y_c ))
      {
	float rad_ra ;
	if(MES_preferences.Pole == SouthPole) rad_ra = (PI*(-curStarInfo->ra_now-90))/180;
	else rad_ra = (PI*(curStarInfo->ra_now-90))/180;
	nb_print_stars++; 
	PrgbImg->draw_arc_circle(floor( x_c ), floor( y_c ), R, rad_ra+PI/6 , rad_ra-PI/6 ,
				 red ,0.4 /*1 opacity*/, 0xFFFFFFFF /*pattern*/);
	
	
	if(MES_preferences.zoom)
	  {
	    CrossSize = floor( Win_Scale*32./curStarInfo->Vmag );
	  } else {
	  CrossSize = floor( Win_Scale*64./curStarInfo->Vmag );
	}
	PrgbImg->draw_circle(floor( std::get<0>(Pos) +x_c+ 0.5), floor( std::get<1>(Pos) +y_c+ 0.5), CrossSize , yellow, 0.4 , 0xFFFFFFFF );
	
	// Draw text
	
	// PrgbImg->draw_text(floor( std::get<0>(Pos) +x_c +0.5),
	//       floor( std::get<1>(Pos) +y_c +0.5),
	//	curStarInfo->Name.c_str(),
	//	red, black , 0.5 /*opacity*/, MES_Display->Font_Size());
      }
  }
  
  if(MES_preferences.zoom)
    {
      Win_Scale = Win_Width; Win_Scale = Win_Scale / img->width();
      int C_x = MES_preferences.zoom_x/Win_Scale -Win_Width/2;
      int C_y = MES_preferences.zoom_y/Win_Scale -Win_Height/2 ;	
      
      if( C_x < 0) C_x = 0;       if( C_y < 0) C_y = 0;
      if( C_x > rgbImg.width() - Win_Width) C_x = rgbImg.width() - Win_Width;
      if( C_y > rgbImg.height() - Win_Height) C_y = rgbImg.height() - Win_Height;
      resized_rgbImg = rgbImg.get_crop(C_x , C_y , C_x + Win_Width, C_y + Win_Height);
    };
  MES_Display->draw_img( &resized_rgbImg );
  return 0;
}

// check there is enough stars and initialize global variable for 
// the rotation center detection algorithm.
bool enough_stars( int img1_stars_size, int img2_stars_size )
{
  if(( img1_stars_size >= (MAX_USE_NB_STARS+MAX_CLOSE_NB_STARS)) 
     && ( img2_stars_size >= (MAX_USE_NB_STARS+MAX_CLOSE_NB_STARS))) 
    {
      USE_NB_STARS = MAX_USE_NB_STARS;
      CLOSE_NB_STARS = MAX_CLOSE_NB_STARS;
    }
  else{
      USE_NB_STARS = MIN_USE_NB_STARS;
      CLOSE_NB_STARS = MIN_CLOSE_NB_STARS;
      MES_Display->info4(
			 MES_preferences.Lang->msg_info4_few_stars().c_str(),
			 img1_stars_size );
  }
  return ( img1_stars_size > (MIN_USE_NB_STARS+MIN_CLOSE_NB_STARS) 
	   && ( img2_stars_size > (MIN_USE_NB_STARS+MIN_CLOSE_NB_STARS))) ;
}

/////////////////////////////////////////////////////////////////


string get_name_img( bool demo, const char *baseimg_name )
{
  static int photo_number = 0;
  string img_name;
  if(demo )
    {
      img_name = baseimg_name;
    }
  else{

    //    char *photo_nupmber_char = itoa(photo_number);
    stringstream ss;
    ss << photo_number;
    string photo_number_str = ss.str();   // string(photo_number_char);    
    img_name = MES_preferences.JpegName + photo_number_str + ".jpg";
    string GetPhoto ;
    GetPhoto = MES_preferences.GetPhoto + " " + img_name;
    cout << " Commande : " << GetPhoto << " image name: " << img_name << endl;
    if( system( GetPhoto.c_str() ) != 0 ) {
      cerr << GetPhoto << " unable to contact camera" << endl;
    }
    photo_number++;
  }
  return img_name;
}


SegmentListT all_Segments;
CImg<unsigned char> *img;
int main(int argc, char *argv[])
{
  int Com = CMD_NONE;
  GetPot   cl(argc, argv);
  const bool  demo = cl.search("-demo");
  const char *baseimg_name   = cl("-baseimg", "");
  const char *img_name   = cl("-img", "");

  if( getenv( "HOME" ) != NULL )
    {
      string Home = getenv( "HOME" );
      string default_config_file = Home + DEFAULT_config_file;
      if( access( default_config_file.c_str() , R_OK )!=-1)
	{
	  GetPot def_cf(default_config_file.c_str());
	  MES_preferences.cl_init( &def_cf );
	} else cerr << default_config_file << " not found" << endl;
    } else cerr << "Home undefined - defautl config file not found" << endl;
  
  if( cl.search(2,"-file" , "-config"))
    {
      const char* config_file = cl.follow("", 2,"-file", "-config"); 
      GetPot cf(config_file);
      MES_preferences.cl_init( &cf );
    };
  MES_preferences.cl_init( &cl );
  MES_Display = new  MES_DisplayT;
  while( Com != CMD_QUIT )
    {
      do{
	MES_Display->info();
	UpdateCatalogue(); // do it now while user turn the scope by hand..
	Com = MES_Display->Wait();
      } while( Com == CMD_REDRAW  );
      if(Com == CMD_PROCESS)
	try {
	  StarInfoT Center1; // , Center2;
	  MES_Display->info_get_image();
	  img = new CImg<unsigned char>( get_name_img( demo, baseimg_name ).c_str() );
	  if(!img) // empty
	    {
	      MES_Display->info4(
				 MES_preferences.Lang->msg_info4_unable().c_str()
				 );
	      delete img;
	    }
	  VERY_SMALL_COMPUTER_resize( img );
	  MES_Display->draw_img( img );
	  MES_Display->info2();
	  StarInfoListT img1_stars = list_stars( (*img) );
	  img1_stars.sort( compare_stars );
	  //      print_list_stars( "img1", &img1_stars );
	  do {
	    Com = MES_Display->Wait();
	  } while( Com == CMD_REDRAW  );
	  
	  delete img;
	  if(Com == CMD_PROCESS)
	    {
	      MES_Display->info_get_image();
	      img = new CImg<unsigned char>( get_name_img( demo, img_name ).c_str() );
	      if(!img) // empty
		{
		  MES_Display->info4(
				     MES_preferences.Lang->msg_info4_unable().c_str()
				     );
		  delete img;
		}
	      VERY_SMALL_COMPUTER_resize( img );
	      MES_preferences.update_time(true);
	      MES_Display->info_get_stars();
	      StarInfoListT img2_stars = list_stars( (*img) );
	      img2_stars.sort( compare_stars );
	      //      print_list_stars( "img2", &img2_stars );
	      
	      if(enough_stars( img1_stars.size(), img2_stars.size()))
		{
		  MES_Display->info_get_center();
		  all_Segments = list_segments( &img1_stars, &img2_stars) ;
		  Center1 = find_center( &all_Segments );
		  
		  //		  all_Segments = list_segments( &img2_stars, &img1_stars) ;
		  //		  Center2 = find_center( &all_Segments );

		  if( isnan( std::get<0>(Center1.cogCentroid ))
		      || isnan( std::get<0>(Center1.cogCentroid ))
		      // || isnan( std::get<0>(Center2.cogCentroid ))
		      // || isnan( std::get<0>(Center2.cogCentroid ))
		      )
		    {
		      MES_Display->info4(
					 MES_preferences.Lang->msg_info4_no_center().c_str()
					 );
		      Com = MES_Display->Wait();
		      if( Com != CMD_QUIT) Com = CMD_RESTART;
		    }
		}
	      else {
		MES_Display->info4(
				   MES_preferences.Lang->msg_info4_no_star().c_str()
				   );
		Com = MES_Display->Wait();
		if( Com != CMD_QUIT) Com = CMD_RESTART;
	      }
	      //       bool Quit = false; char c;

	      MES_preferences.zoom = false;
	      // draw the first image (and add info3 message)	      
	      if( (Com != CMD_QUIT) && (Com != CMD_RESTART)  )
		do {
		  draw_img_liste_stars( img, Center1, &Catalogue);
		  MES_Display->info3(  );
		  Com = MES_Display->Wait( MES_preferences.wait_millisec );
		} while( Com == CMD_REDRAW );
	      delete img;
	
	      // draw the following images (wthout messages)	      
	      while( (Com != CMD_QUIT) && (Com != CMD_RESTART)  )
		{
		  img = new CImg<unsigned char>( get_name_img( demo, img_name ).c_str() );
		  if(!img) // empty
		    {
		      MES_Display->info4(
					 MES_preferences.Lang->msg_info4_unable().c_str()
					 );
		      delete img;
		    }
		  VERY_SMALL_COMPUTER_resize( img );
		  MES_preferences.update_time(false);
		  do {
		    draw_img_liste_stars( img, Center1, &Catalogue);
		    Com = MES_Display->Wait( MES_preferences.wait_millisec );
		  } while( Com == CMD_REDRAW );
		  delete img;
		}
	    }
	  //	} catch (CImgIOException&) {
	} catch (...) {
	  MES_Display->info4(
			     MES_preferences.Lang->msg_info4_unable().c_str()
			     );
	  Com = MES_Display->Wait( MES_preferences.wait_millisec );
	}
    }
  return 0;
}


