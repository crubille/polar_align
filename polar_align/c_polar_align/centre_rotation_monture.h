/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2015 Carsten Schmitt
 * Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compiègne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// CONFIGURABLE 
const float THRESHOLD_VALUE = 96. ;

#ifdef VERY_SMALL_COMPUTER
const int MAX_USE_NB_STARS= 70; // ENOUGH TO GET A GOOD DETECTION
const int MAX_CLOSE_NB_STARS= 30; // AND MORE THAN THAT IS COSTLY
#else
const int MAX_USE_NB_STARS= 150; // ENOUGH TO GET A GOOD DETECTION
const int MAX_CLOSE_NB_STARS= 50; // AND MORE THAN THAT IS COSTLY
#endif
const int MIN_USE_NB_STARS= 20; // ALLOW TO COMPUTE EVEN WITH FEW DETECTED STARS
const int MIN_CLOSE_NB_STARS= 10; // DETECTION

int USE_NB_STARS;
int CLOSE_NB_STARS; // ASSERT 2*CLOSE_NB_STARS > USE_NB_STARS

void
thresholdOtsu(const CImg<unsigned char> & inImg, long inBitPix, CImg<unsigned char> * outBinImg)
{
  CImg<> hist = inImg.get_histogram(pow(2.0, inBitPix));
 
  float sum = 0;
  cimg_forX(hist, pos) { sum += pos * hist[pos]; }
 
  float numPixels = inImg.width() * inImg.height();
  float sumB = 0, wB = 0, max = 0.0;
  float threshold1 = 0.0, threshold2 = 0.0;
  
  cimg_forX(hist, i) {
    wB += hist[i];
 
    if (! wB) { continue; }    
 
    float wF = numPixels - wB;
    
    if (! wF) { break; }
    
    sumB += i * hist[i];
 
    float mF = (sum - sumB) / wF;
    float mB = sumB / wB;
    float diff = mB - mF;
    float bw = wB * wF * pow(diff, 2.0);
    
    if (bw >= max) {
      threshold1 = i;
      if (bw > max) {
         threshold2 = i;
      }
      max = bw;            
    }
  } // end loop
  
  float th = (threshold1 + threshold2) / 2.0;
 
  *outBinImg = inImg; // Create a copy
  outBinImg->threshold(th); 
}
 
/**
 * Removes all white neighbours arond pixel from whitePixels
 * if they exist and adds them to pixelsToBeProcessed.
 */
void
getAndRemoveNeighbours(PixelPosT inCurPixelPos, PixelPosSetT * inoutWhitePixels, PixelPosListT * inoutPixelsToBeProcessed)
{
  const size_t _numPixels = 8, _x = 0, _y = 1;
  const int offsets[_numPixels][2] = { { -1, -1 }, { 0, -1 }, { 1, -1 },
                                       { -1, 0 },              { 1, 0 },
                                       { -1, 1 }, { 0, 1 }, { 1, 1 } };
  
  for (size_t p = 0; p < _numPixels; ++p) {
    PixelPosT curPixPos(std::get<0>(inCurPixelPos) + offsets[p][_x], std::get<1>(inCurPixelPos) + offsets[p][_y]);
    PixelPosSetT::iterator itPixPos = inoutWhitePixels->find(curPixPos);
 
    if (itPixPos != inoutWhitePixels->end()) {
      const PixelPosT & curPixPos = *itPixPos;
      inoutPixelsToBeProcessed->push_back(curPixPos);
      inoutWhitePixels->erase(itPixPos); // Remove white pixel from "white set" since it has been now processed
    }
  }
  return;
}
 
template<typename T> void
clusterStars(const CImg<T> & inImg, StarInfoListT * outStarInfos)
{
  PixelPosSetT whitePixels;
 
  cimg_forXY(inImg, x, y) {
    if (inImg(x, y)) {
      whitePixels.insert(whitePixels.end(), PixelPosT(x, y));
      if(whitePixels.size() > 1000000 ) return; // to prevent very very long delay for a daylight photo
    }
  }
 
  // Iterate over white pixels as long as set is not empty
  while (whitePixels.size()) {
    PixelPosListT pixelsToBeProcessed;
 
    PixelPosSetT::iterator itWhitePixPos = whitePixels.begin();
    pixelsToBeProcessed.push_back(*itWhitePixPos);
    whitePixels.erase(itWhitePixPos);
 
    FrameT frame(inImg.width(), inImg.height(), 0, 0);
 
    while(! pixelsToBeProcessed.empty()) {
      PixelPosT curPixelPos = pixelsToBeProcessed.front();
 
      // Determine boundaries (min max in x and y directions)
      if (std::get<0>(curPixelPos) /*x*/ < std::get<0>(frame) /*x1*/) {    std::get<0>(frame) = std::get<0>(curPixelPos); }
      if (std::get<0>(curPixelPos) /*x*/ > std::get<2>(frame) /*x2*/) { std::get<2>(frame) = std::get<0>(curPixelPos); }
      if (std::get<1>(curPixelPos) /*y*/ < std::get<1>(frame) /*y1*/) {    std::get<1>(frame) = std::get<1>(curPixelPos); }
      if (std::get<1>(curPixelPos) /*y*/ > std::get<3>(frame) /*y2*/) { std::get<3>(frame) = std::get<1>(curPixelPos); }
 
      getAndRemoveNeighbours(curPixelPos, & whitePixels, & pixelsToBeProcessed);
      pixelsToBeProcessed.pop_front();
    }
 
    // Create new star-info and set cluster-frame.
    // NOTE: we may use new to avoid copy of StarInfoT...
    StarInfoT starInfo;
    starInfo.clusterFrame = frame;
    outStarInfos->push_back(starInfo);
  }
}
 
float
calcIx2(const CImg<unsigned char> & img, int x)
{
  float Ix = 0;
  cimg_forY(img, y) { Ix += pow((float)img(x, y), 2.0) * (float) x; }
  return Ix;
}
 
float
calcJy2(const CImg<unsigned char> & img, int y)
{
  float Iy = 0;
  cimg_forX(img, x) { Iy += pow((float)img(x, y), 2.0) * (float) y; }
  return Iy;
}
 
// Calculate Intensity Weighted Center (IWC)
void
calcIntensityWeightedCenter(const CImg<unsigned char> & inImg, float * outX, float * outY)
{
  assert(outX && outY);
  
  // Determine weighted centroid - See http://cdn.intechopen.com/pdfs-wm/26716.pdf
  float Imean2 = 0, Jmean2 = 0, Ixy2 = 0;
  
  for(size_t i = 0; i < inImg.width(); ++i) {
    Imean2 += calcIx2(inImg, i);
    cimg_forY(inImg, y) { Ixy2 += pow(inImg(i, y), 2.0); }
  }
 
  for(size_t i = 0; i < inImg.height(); ++i) {
    Jmean2 += calcJy2(inImg, i);
  }
  
  *outX = Imean2 / Ixy2;
  *outY = Jmean2 / Ixy2;
}
 
void
calcSubPixelCenter(const CImg<unsigned char> & inImg, float * outX, float * outY, size_t inNumIter = 10 /*num iterations*/)
{
  // Sub pixel interpolation
  float c, a1, a2, a3, a4, b1, b2, b3, b4;
  float a1n, a2n, a3n, a4n, b1n, b2n, b3n, b4n;
 
  assert(inImg.width() == 3 && inImg.height() == 3);
 
  b1 = inImg(0, 0); a2 = inImg(1, 0); b2 = inImg(2, 0);
  a1 = inImg(0, 1);  c = inImg(1, 1); a3 = inImg(2, 1);
  b4 = inImg(0, 2); a4 = inImg(1, 2); b3 = inImg(2, 2);
 
  for (size_t i = 0; i < inNumIter; ++i) {
    float c2 = 2 * c;
    float sp1 = (a1 + a2 + c2) / 4;
    float sp2 = (a2 + a3 + c2) / 4;
    float sp3 = (a3 + a4 + c2) / 4;
    float sp4 = (a4 + a1 + c2) / 4;
    
    // New maximum is center
    float newC = std::max({ sp1, sp2, sp3, sp4 });
    
    // Calc position of new center
    float ad = pow(2.0, -((float) i + 1));
 
    if (newC == sp1) {
      *outX = *outX - ad; // to the left
      *outY = *outY - ad; // to the top
 
      // Calculate new sub pixel values
      b1n = (a1 + a2 + 2 * b1) / 4;
      b2n = (c + b2 + 2 * a2) / 4;
      b3n = sp3;
      b4n = (b4 + c + 2 * a1) / 4;
      a1n = (b1n + c + 2 * a1) / 4;
      a2n = (b1n + c + 2 * a2) / 4;
      a3n = sp2;
      a4n = sp4;
 
    } else if (newC == sp2) {
      *outX = *outX + ad; // to the right
      *outY = *outY - ad; // to the top
 
      // Calculate new sub pixel values
      b1n = (2 * a2 + b1 + c) / 4;
      b2n = (2 * b2 + a3 + a2) / 4;
      b3n = (2 * a3 + b3 + c) / 4;
      b4n = sp4;
      a1n = sp1;
      a2n = (b2n + c + 2 * a2) / 4;
      a3n = (b2n + c + 2 * a3) / 4;
      a4n = sp3;
    } else if (newC == sp3) {
      *outX = *outX + ad; // to the right
      *outY = *outY + ad; // to the bottom
 
      // Calculate new sub pixel values
      b1n = sp1;
      b2n = (b2 + 2 * a3 + c) / 4;
      b3n = (2 * b3 + a3 + a4) / 4;
      b4n = (2 * a4 + b4 + c) / 4;
      a1n = sp4;
      a2n = sp2;
      a3n = (b3n + 2 * a3 + c) / 4;
      a4n = (b3n + 2 * a4 + c) / 4;
    } else {
      *outX = *outX - ad; // to the left
      *outY = *outY + ad; // to the bottom  
 
      // Calculate new sub pixel values
      b1n = (2 * a1 + b1 + c) / 4;
      b2n = sp2;
      b3n = (c + b3 + 2 * a4) / 4;
      b4n = (2 * b4 + a1 + a4) / 4;
      a1n = (b4n + 2 * a1 + c) / 4;
      a2n = sp1;
      a3n = sp3;
      a4n = (b4n + 2 * a4 + c) / 4;
    }
 
    c = newC; // Oi = Oi+1
 
    a1 = a1n;
    a2 = a2n;
    a3 = a3n;
    a4 = a4n;
 
    b1 = b1n;
    b2 = b2n;
    b3 = b3n;
    b4 = b4n;
  }
}
 
void
calcCentroid(const CImg<unsigned char> & inImg, const FrameT & inFrame, PixSubPosT * outPixelPos, PixSubPosT * outSubPixelPos = 0, size_t inNumIterations = 10)
{
  // Get frame sub img
  CImg<unsigned char> subImg = inImg.get_crop(std::get<0>(inFrame), std::get<1>(inFrame), std::get<2>(inFrame), std::get<3>(inFrame));
 
  float & xc = std::get<0>(*outPixelPos);
  float & yc = std::get<1>(*outPixelPos);
  
  // 1. Calculate the IWC
  calcIntensityWeightedCenter(subImg, & xc, & yc);
 
  if (outSubPixelPos) {
    // 2. Round to nearest integer and then iteratively improve.
    int xi = floor(xc + 0.5);
    int yi = floor(yc + 0.5);
  
    CImg<unsigned char> img3x3 = inImg.get_crop(xi - 1 /*x0*/, yi - 1 /*y0*/, xi + 1 /*x1*/, yi + 1 /*y1*/);
    
    // 3. Interpolate using sub-pixel algorithm
    float xsc = xi, ysc = yi;
    calcSubPixelCenter(img3x3, & xsc, & ysc, inNumIterations);
    
    std::get<0>(*outSubPixelPos) = xsc;
    std::get<1>(*outSubPixelPos) = ysc;
  }
}
 
 
/**
* Expects star centered in the middle of the image (in x and y) and mean background subtracted from image.
*
* HDF calculation: http://www005.upp.so-net.ne.jp/k_miyash/occ02/halffluxdiameter/halffluxdiameter_en.html
*                  http://www.cyanogen.com/help/maximdl/Half-Flux.htm
*
* NOTE: Currently the accuracy is limited by the insideCircle function (-> sub-pixel accuracy).
* NOTE: The HFD is estimated in case there is no flux (HFD ~ sqrt(2) * inOuterDiameter / 2).
* NOTE: The outer diameter is usually a value which depends on the properties of the optical
*       system and also on the seeing conditions. The HFD value calculated depends on this
*       outer diameter value.
*/
float
calcHfd(const CImg<unsigned char> & inImage, unsigned int inOuterDiameter)
{
  // Sum up all pixel values in whole circle
  float outerRadius = inOuterDiameter / 2;
  float sum = 0, sumDist = 0;
  int centerX = ceil(inImage.width() / 2.0);
  int centerY = ceil(inImage.height() / 2.0);
 
  cimg_forXY(inImage, x, y) {
    if (insideCircle(x, y, centerX, centerY, outerRadius)) {
      sum += inImage(x, y);
      sumDist += inImage(x, y) * sqrt(pow((float) x - (float) centerX, 2.0f) + pow((float) y - (float) centerY, 2.0f));
    }
  }
  // NOTE: Multiplying with 2 is required since actually just the HFR is calculated above
  return (sum ? 2.0 * sumDist / sum : sqrt(2.0) * outerRadius);
}
 
/**********************************************************************
* Helper classes
**********************************************************************/
struct DataPointT {
  float x;
  float y;
  DataPointT(float inX = 0, float inY = 0) : x(inX), y(inY) {}
};
  
typedef vector<DataPointT> DataPointsT;
  
struct GslMultiFitDataT {
  float y;
  float sigma;
  DataPointT pt;
};
  
typedef vector<GslMultiFitDataT> GslMultiFitParmsT;
 
 
typedef list<PixSubPosT> MyDataContainerT;
 
class MyDataAccessorT {
public:
  typedef MyDataContainerT TypeT;
  static DataPointT getDataPoint(size_t inIdx, TypeT::const_iterator inIt) {
    const PixSubPosT & pos = *inIt;
    DataPointT dp(get<0>(pos) /*inIdx*/, get<1>(pos) /*y*/);
    return dp;
  }
};
 
 
FrameT
rectify(const FrameT & inFrame)
{
  float border = 3;
  float border2 = 2.0 * border;
  float width = fabs(std::get<0>(inFrame) - std::get<2>(inFrame)) + border2;
  float height = fabs(std::get<1>(inFrame) - std::get<3>(inFrame)) + border2;
  float L = max(width, height);
  float x0 = std::get<0>(inFrame) - (fabs(width - L) / 2.0) - border;
  float y0 = std::get<1>(inFrame) - (fabs(height - L) / 2.0) - border;
  return FrameT(x0, y0, x0 + L, y0 + L);
}
 

StarInfoListT list_stars( CImg<unsigned char> & img )
{
/* outerHfdDiameter depends on pixel size and focal length (and seeing...).
     Later we may calculate it automatically wihth goven focal length and pixel
     size of the camera. For now it is a "best guess" value.
  */
  const unsigned int outerHfdDiameter = 21;
  StarInfoListT starInfos;
  //  vector < list<StarInfoT *> > starBuckets;
  long bitPix = 0;
 
  img.normalize(0, 255 );
  // Thresholding but only down to black ...
  cimg_forXY( img , x, y) {
    if( img(x,y) < THRESHOLD_VALUE )   { img(x,y) = 0.;}
  }

  // Clustering --> In: binary image from thresholding, Out: List of detected stars, subimg-boundaries (x1,y1,x2,y2) for each star
  //  clusterStars(binImg, & starInfos);
  clusterStars( img, & starInfos);
 
  
  cerr << "Recognized " << starInfos.size() << " stars..." << endl;
 
  // Calc brightness boundaries for possible focusing stars
  float maxPossiblePixValue = pow(2.0, bitPix) - 1;
 
  // For each star
  for (StarInfoListT::iterator it = starInfos.begin(); it != starInfos.end(); ++it) {
    const FrameT & frame = it->clusterFrame;
    FrameT & cogFrame = it->cogFrame;
    FrameT & hfdFrame = it->hfdFrame;
    PixSubPosT & cogCentroid = it->cogCentroid;
    PixSubPosT & subPixelInterpCentroid = it->subPixelInterpCentroid;
    float & hfd = it->hfd;
    float & maxPixValue = it->maxPixValue;
    
    FrameT squareFrame = rectify(frame);
    
    // Centroid calculation --> In: Handle to full noise reduced image, subimg-boundaries (x1,y1,x2,y2), Out: (x,y) - abs. centroid coordinates
    calcCentroid( img, squareFrame, & cogCentroid, & subPixelInterpCentroid, 10 /* num iterations */);
    std::get<0>(cogCentroid) += std::get<0>(squareFrame);
    std::get<1>(cogCentroid) += std::get<1>(squareFrame);
    std::get<0>(subPixelInterpCentroid) += std::get<0>(squareFrame);
    std::get<1>(subPixelInterpCentroid) += std::get<1>(squareFrame);
    
    
    // Calculate cog boundaries
    float maxClusterEdge = std::max(fabs(std::get<0>(frame) - std::get<2>(frame)), fabs(std::get<1>(frame) - std::get<3>(frame)));
    float cogHalfEdge = ceil(maxClusterEdge / 2.0);
    float cogX = std::get<0>(cogCentroid);
    float cogY = std::get<1>(cogCentroid);
    std::get<0>(cogFrame) = cogX - cogHalfEdge - 1;
    std::get<1>(cogFrame) = cogY - cogHalfEdge - 1;
    std::get<2>(cogFrame) = cogX + cogHalfEdge + 1;
    std::get<3>(cogFrame) = cogY + cogHalfEdge + 1;
 
    
    // HFD calculation --> In: image, Out: HFD value
    // Subtract mean value from image which is required for HFD calculation
    size_t hfdRectDist = floor(outerHfdDiameter / 2.0);
    std::get<0>(hfdFrame) = cogX - hfdRectDist;
    std::get<1>(hfdFrame) = cogY - hfdRectDist;
    std::get<2>(hfdFrame) = cogX + hfdRectDist;
    std::get<3>(hfdFrame) = cogY + hfdRectDist;
 
    CImg<unsigned char> hfdSubImg = img.get_crop(std::get<0>(hfdFrame), std::get<1>(hfdFrame), std::get<2>(hfdFrame), std::get<3>(hfdFrame));
    maxPixValue = hfdSubImg.max();
    
    CImg<unsigned char> imgHfdSubMean(hfdSubImg);
    double mean = hfdSubImg.mean();
 
    cimg_forXY(hfdSubImg, x, y) {
      imgHfdSubMean(x, y) = (hfdSubImg(x, y) < mean ? 0 : hfdSubImg(x, y) - mean);
    }
 
    // Calc the HFD
    hfd = calcHfd(imgHfdSubMean, outerHfdDiameter /*outer diameter in px*/);
  }

  return starInfos ;
}

   StarInfoT moyenne( StarInfoListT *IntList)
  {
    StarInfoT m;
    PixSubPosT c;  
    float Xm, Ym;
    Xm=0; Ym=0;
    for (StarInfoListT::iterator it = IntList->begin(); it != IntList->end(); ++it)
      {
	c = (*it).cogCentroid;
	Xm += std::get<0>(c);
	Ym += std::get<1>(c);
      }
    Xm = Xm/IntList->size();
    Ym = Ym/IntList->size();

    m.cogCentroid = make_tuple( Xm, Ym );
    return m;
    
  }
    

SegmentListT list_segments( StarInfoListT *img1_stars, StarInfoListT *img2_stars )
{
	  SegmentListT all_Segments;
	  int i;
	  StarInfoListT FinSegments;
	  StarInfoListT::iterator it_img1_stars, it_img2_stars, it_segments;
	  // compute the segments from the USE_NB_STARS first stars in img1
	  //                        to CLOSE_NB_STARS in img2
	  //  which have similar maxPixel*hfd the same position.
	  // so i get USE_NB_STARS*CLOSE_NB_STARS segments
	  // check that we found more than USE_NB_STARS in each images.:
	  all_Segments.clear();
	  FinSegments.clear();
	  it_img2_stars = img2_stars->begin();
	  for( i=0; i< (CLOSE_NB_STARS-1)/2 ; i++)
	    {
	      FinSegments.push_back( (*it_img2_stars) ); // respect order...
	      it_img2_stars++;
	    };
	  it_img1_stars = img1_stars->begin();
	  for( i= 0; i < USE_NB_STARS ; i++)
	    {

	      for( it_segments = FinSegments.begin(); it_segments != FinSegments.end() ; it_segments++)
		{// add a segment from a star from img1 to a star from img2
		  SegmentT S;
		  // S.begin_pos = (*it_img1_stars).subPixelInterpCentroid;
		  S.begin_pos = (*it_img1_stars).cogCentroid;
		  S.begin_hfd = (*it_img1_stars).hfd;
		  S.begin_maxPixValue = (*it_img1_stars).maxPixValue;

		  //S.end_pos = (*it_segments).subPixelInterpCentroid;
		  S.end_pos = (*it_segments).cogCentroid;
		  S.end_hfd = (*it_segments).hfd;
		  S.end_maxPixValue = (*it_segments).maxPixValue;

		  all_Segments.push_back( S );
		};
	      
	      FinSegments.push_back( (*it_img2_stars) ); // respect order...
	      it_img2_stars++;
	      if(FinSegments.size() > CLOSE_NB_STARS )
		{
		  FinSegments.pop_front(); // fifo ...
		}
	      it_img1_stars++;
	    };  
  return all_Segments ;
}

void find_segment_intersection(StarInfoListT *PerSegmentIntersectionList, SegmentListT::iterator it_Seg1, SegmentListT *all_Segments )
{
  static SegmentListT::iterator it_Seg2; 
  StarInfoT Intersection;

  PerSegmentIntersectionList->clear() ;
  // find all credible intersections
  float xo1, yo1, xb1 , yb1;// first segment
  xo1 =std::get<0>((*it_Seg1).begin_pos); yo1 =std::get<1>((*it_Seg1).begin_pos);
  xb1 =std::get<0>((*it_Seg1).end_pos); yb1 =std::get<1>((*it_Seg1).end_pos);


  for( it_Seg2 = it_Seg1; it_Seg2 != all_Segments->end(); it_Seg2++)
    {
      if( ( (*it_Seg1).begin_pos == (*it_Seg2).begin_pos) ||
	  ( (*it_Seg1).end_pos == (*it_Seg2).end_pos) )
	{ /* do nothing */ } else {
	// found the bisectrice intersection:
	float X,Y; //intersection coord ...
	float xo2, yo2, xb2 , yb2;//second segment
	float A, B, C, D;
	
	xo2 =std::get<0>((*it_Seg2).begin_pos); yo2 =std::get<1>((*it_Seg2).begin_pos);
	xb2 =std::get<0>((*it_Seg2).end_pos); yb2 =std::get<1>((*it_Seg2).end_pos);
	
	A = xo1-xb1; B = xo2-xb2; C = yo1-yb1, D = yo2-yb2;
	if( fabs(A*D-B*C) < 10. ) { /* parrallel */ } else {
	  // compute bisectrice intersection
	  X = A*D*(xo1+xb1) - B*C*(xo2+xb2) + C*D*(yo1+yb1) - C*D*(yo2+yb2);
	  X = X / (2*(A*D-B*C));
	  Y = B*(xo2+xb2) +D*(yo2+yb2) -2*B*X;
	  Y = Y /(2*D);
	  // check intersection is a credible rotation center:
	  {
	    float pvs, pv, pv1,  pv2, rd, rd1, rd2;
	    pv1 = x_vector( X-xo1,Y-yo1 , X-xb1,Y-yb1)/distance2(xo1,yo1 , xb1,yb1);
	    pv2 = x_vector( X-xo2,Y-yo2 , X-xb2,Y-yb2)/distance2(xo2,yo2 , xb2,yb2);
	    if( (fabs(pv1)*0.98 < fabs(pv2)) && ( fabs(pv2) < fabs(pv1*1.02 ))
		&& (0.7 < fabs(pv1) ) && (fabs(pv1) < 3) // ratio include rotation in 30-60 degres.
		)
	      {
		pv = (pv1 + pv2 )/2.;
		pvs = fabs( x_vector( xb1-xo1,yb1-yo1 , xb2-xo2,yb2-yo2)); // aire des "vecteurs" des 2 segments" divisé par carré des longueurs
		rd = (rd1+rd2)/2;
		
		// cout << pv <<" = corde/rayon de intersection ok X=" << X << " Y="<< Y << " pvs =" << pvs << endl;
		Intersection.rotation = pv;
		Intersection.quality = pvs;
		Intersection.cogCentroid = make_tuple(X,Y);
		PerSegmentIntersectionList->push_back( Intersection );
	      }
	  }
	}
      }
    }
}
StarInfoT find_center( SegmentListT *all_Segments )
{
  	  // NOW compute all intersection between segments ...
	  //  ==      (USE_NB_STARS*CLOSE_NB_STARS)^2
  static StarInfoListT IntersectionList, PerStarIntersectionList, PerSegmentIntersectionList;
  StarInfoT midListIntersection, Center;
  StarInfoListT::iterator it_Intersection;
  SegmentListT::iterator it_Seg1 ;
  int i;
  float old_xo1=0, old_yo1=0;

  for( it_Seg1 = all_Segments->begin(); it_Seg1 != all_Segments->end() ; it_Seg1++)
    {
      if( (old_xo1 != std::get<0>((*it_Seg1).begin_pos))
       || (old_yo1 != std::get<1>((*it_Seg1).begin_pos) ))
	{ // first star change 
	  //	  print_list_stars( "liste_segment selectionnée", &PerStarIntersectionList);
	  old_xo1 = std::get<0>((*it_Seg1).begin_pos);
	  old_yo1 =std::get<1>((*it_Seg1).begin_pos);
	  IntersectionList.merge( PerStarIntersectionList, compare_intersections) ;
	  PerStarIntersectionList.clear();
	}

      // find all credible intersections
      find_segment_intersection( &PerSegmentIntersectionList, it_Seg1, all_Segments );
      if ( PerSegmentIntersectionList.size() >= 5 )
	{
	  PerSegmentIntersectionList.sort( compare_intersections );
	  // check these intersections list:
	  it_Intersection = PerSegmentIntersectionList.begin();
	  for( i = 0;
	       i < PerSegmentIntersectionList.size()/2;
	       i++ ) {it_Intersection ++;}
	  midListIntersection = (*it_Intersection);

	  int first_ok, last_ok;
	  i = 0; first_ok = PerSegmentIntersectionList.size()/2; last_ok = first_ok;
	  for( it_Intersection = PerSegmentIntersectionList.begin();
	       it_Intersection != PerSegmentIntersectionList.end();
	       it_Intersection ++)
	    {
	      if( assez_proche( midListIntersection, (*it_Intersection) ))
		{
		  if( i < first_ok ) first_ok = i;
		  if( last_ok < i )  last_ok  = i;
		};
		i++;
	    }
	  if(( PerStarIntersectionList.size() < (last_ok-first_ok) )
	     && ( last_ok-first_ok > 5) )
	    {
	      if( PerStarIntersectionList.size()>(PerSegmentIntersectionList.size()/2 ))
		{ // A COMPLETER
		  PerSegmentIntersectionList.clear();
		  cerr << endl << "Ambiguité pour une étoile " << endl ;
		}
		PerStarIntersectionList.clear();

		// do not exactly PerStarIntersectionList.merge( PerSegmentIntersectionList, compare_intersections) ;
		it_Intersection = PerSegmentIntersectionList.begin();
		for( i =0 ; i< PerSegmentIntersectionList.size() ; i++)
		  {
		    if( first_ok <= i && i <= last_ok)
		      {
			PerStarIntersectionList.push_back( (*it_Intersection) );
		      }
		    it_Intersection++;
		  }

		}
	  else 
	    { // no or too few intersections founds
	    }
	}
      else{ /* not enough intersections found to do anything */ }
     }
  //  print_list_stars( "liste intersections", &IntersectionList);
  // compute mid value for X and Y
  cout << endl << "Nombre d'intersection trouvée : " << IntersectionList.size() << endl ;
  Center = moyenne( &IntersectionList);
	  cout << endl << "Center approx is ("
	       << std::get<0>(Center.cogCentroid)
	       << ", "
	       << std::get<1>(Center.cogCentroid)
	       << endl;
  // reject out of range intersection
  it_Intersection = IntersectionList.begin();
  while( it_Intersection != IntersectionList.end() ) 
    {
      if( too_long( Center, (*it_Intersection) )) 
      	{
	  IntersectionList.erase( it_Intersection );
	  it_Intersection = IntersectionList.begin();
	} else
	it_Intersection++;
    }
  // re compute mid value for X and Y
  cout << endl << "Nombre d'intersection restante : " << IntersectionList.size() << endl ;
  Center = moyenne( &IntersectionList);
	  cout << endl << "Center result is ("
	       << std::get<0>(Center.cogCentroid)
	       << ", "
	       << std::get<1>(Center.cogCentroid)
	       << endl;
  return Center;
}
