/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2020 Paul Crubillé Sorbonne universités, Université de Technologie de Compiègne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compiègne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class MES_DisplayT
{
  
  CImgDisplay *sky_disp;
  CImg<unsigned char> *rgbImg;
  int Screen_Height;
  int Screen_Width;
  int font_size;

 public:
  int Win_Width, Win_Height;
 public:
  MES_DisplayT()
    {
      Win_Width = MES_preferences.Screen_Image_Width;
      Win_Height = (2./3.)*MES_preferences.Screen_Image_Width;
      rgbImg = new CImg<unsigned char>(Win_Width,
				       Win_Height, 
				 1 /*depth*/, 3 /*3 channels - RGB*/);

      sky_disp = new CImgDisplay( (*rgbImg),"q to quit", 3, false, false);

      Screen_Height = sky_disp->screen_height();
      Screen_Width  = sky_disp->screen_width() ;
      if( MES_preferences.fullscreen)
	{
	  if(Screen_Height < (2./3.)*Screen_Width)
	    {
	      Win_Width = Screen_Height*3./2.;
	      Win_Height = Screen_Height;
	    }
	  else
	    {
	      Win_Width = Screen_Width;
	      Win_Height = Screen_Width*2./3.;
	    }
	  rgbImg->resize(Win_Width, Win_Height);
	  sky_disp->resize(Win_Width, Win_Height, true );
	  sky_disp->toggle_fullscreen( true );
	}
      else {
	sky_disp->resize(Win_Width, Win_Height, true );
      }
      font_size = ((MES_preferences.font_size*Win_Width)/1280);
    }

#define CMD_NONE       0
#define CMD_QUIT       1
#define CMD_RESTART    2
#define CMD_PROCESS    3
#define CMD_REDRAW     4
  
  int Font_Size()  { return font_size; }
  int  win_width() { return Win_Width;}
  int Wait()
  {    return Wait(0);   }
  int Wait(int msec)
  {
    while( true )
      {
	// if(msec == 0 ) sky_disp->wait();else sky_disp->wait(msec);
	sky_disp->wait();
	if( sky_disp->is_keyQ() || sky_disp->is_closed())
	  return CMD_QUIT;
	if( sky_disp->is_keyR())
	  return CMD_RESTART;
	if ( sky_disp->is_keyESC() )
	  {
	    MES_preferences.zoom = false;
	    return CMD_PROCESS;
	  }
	if ( sky_disp->is_keyENTER() || sky_disp->is_keySPACE() ||
	     sky_disp->is_keyY() || sky_disp->is_keyC())
	  return CMD_PROCESS;
	if( sky_disp->is_keyZ() || (sky_disp->button() == 0x1 ))
	  {
	    if( MES_preferences.zoom )
	      {
		float wx = sky_disp->mouse_x();
		float wy = sky_disp->mouse_y();
		
		MES_preferences.zoom_x -= (Win_Width/2. - wx ) * MES_preferences.Win_Scale ;
		MES_preferences.zoom_y -= (Win_Height/2 - wy ) * MES_preferences.Win_Scale ;
	      }
	    else {
	      MES_preferences.zoom = true;
	      MES_preferences.zoom_x = sky_disp->mouse_x();;
	      MES_preferences.zoom_y = sky_disp->mouse_y();;
	    }
	    return CMD_REDRAW;
	  }
	if(( sky_disp->is_keyU()) || (sky_disp->button() == 0x2))
	  {
	    MES_preferences.zoom = false;
	    return CMD_REDRAW;
	  }
	if( sky_disp->is_keyF())
	  {
	    MES_preferences.fullscreen = !MES_preferences.fullscreen ;
	    return CMD_REDRAW;
	  }
	if( sky_disp->is_keyPADADD())
	  {
	    if(MES_preferences.zoom)
	      MES_preferences.rotate = MES_preferences.rotate +1./20. ; //3arc mn
	    else MES_preferences.rotate++;
	  return CMD_REDRAW;
	  }
	if( sky_disp->is_keyPADSUB())
	  {
	    if(MES_preferences.zoom)
	      MES_preferences.rotate = MES_preferences.rotate - 1./20. ; //3arc mn
	    else MES_preferences.rotate--;
	    return CMD_REDRAW;
	  }
      }
    return CMD_QUIT; 
  }
  void info()
  {
    cimg_forXY((*rgbImg), x, y) {
      (*rgbImg)(x, y, 0 /*red*/) = 0;
      (*rgbImg)(x, y, 1 /*green*/) = 0;
      (*rgbImg)(x, y, 2 /*blue*/) = 0;
    }

    rgbImg->draw_text( 0.05*Win_Width , 0.1*Win_Height,
		       MES_preferences.Lang->window_title().c_str(),
		     red , black , 0.5 , 3*font_size);

    rgbImg->draw_text( 0.05*Win_Width , 0.25*Win_Height,
		       MES_preferences.Lang->info_0().c_str(),
		       red , black , 0.8 , font_size);

    rgbImg->draw_text( 0.05*Win_Width , 0.5*Win_Height,
		       MES_preferences.Lang->info_1().c_str(),
		       red , black , 0.5 , 1.5*font_size);

    rgbImg->draw_text( 0.05*Win_Width , 0.55*Win_Height,
		       MES_preferences.Lang->info_1_1().c_str(),
		       red , black , 0.8 , font_size);

    rgbImg->draw_text( 0.05*Win_Width , 0.65*Win_Height,
		       MES_preferences.Lang->info_1_2().c_str(),
		       red , black , 0.8 , font_size );

    rgbImg->draw_text( 0.05*Win_Width , 0.75*Win_Height,
		       MES_preferences.Lang->info_1_3().c_str(),
		       red , black , 0.8 , font_size );


    rgbImg->draw_text( 0.05*Win_Width , 0.85*Win_Height,
		       MES_preferences.Lang->info_1_4().c_str(),
		       red , black , 0.8 , 1.*font_size );
    
    
    int centerX= Win_Width*0.86;
    int centerY= Win_Height*0.18;
    rgbImg->draw_arc_circle( centerX,centerY, Win_Width/12+20 ,0.32*PI , 1.23*PI, yellow, 1 /*density*/, 0xffffffff
			 /*pattern*/);

    rgbImg->draw_text( 0.8*Win_Width , 0.1*Win_Height,
		       MES_preferences.Lang->info_1_status().c_str(),
		       red , black , 0.8 , font_size);

    sky_disp->display( (*rgbImg) ) ;
  }
  
  void info2() // print message after i get and display the first image
  {
    stringstream s1;
    stringstream s2;
    rgbImg->draw_text( 0.05*Win_Width , 0.05*Win_Height,
		       MES_preferences.Lang->window_title().c_str(),
		     red , black , 0.4 , 3*font_size);

    rgbImg->draw_text( 0.05*Win_Width , 0.15*Win_Height,
		       MES_preferences.Lang->info2().c_str(),
		       red , black , 0.6 , font_size);


    rgbImg->draw_text( 0.05*Win_Width , 0.3*Win_Height,
		       MES_preferences.Lang->info_2_0().c_str(),
		       red , black , 0.5 , 1.5*font_size);
    
    rgbImg->draw_text( 0.05*Win_Width , 0.35*Win_Height,
		       MES_preferences.Lang->info_2_1().c_str(),
		       red , black , 0.6 , font_size);

    rgbImg->draw_text( 0.05*Win_Width , 0.82*Win_Height,
		       MES_preferences.Lang->info_2_2().c_str(),
		       red , black , 0.5 , font_size );

    sky_disp->display( (*rgbImg) ) ;
  }

  void info3() // print message after i get and display the first image
  {
    stringstream s1;
    stringstream s2;
    rgbImg->draw_text( 0.05*Win_Width , 0.05*Win_Height,
		       MES_preferences.Lang->info_3().c_str(),
		       red , black , 0.4 , 3*font_size);

        rgbImg->draw_text( 0.05*Win_Width , 0.15*Win_Height,
		       MES_preferences.Lang->info_3_00().c_str(),
		       red , black , 0.5 , 1.5*font_size);

	rgbImg->draw_text( 0.05*Win_Width , 0.3*Win_Height,
			   MES_preferences.Lang->info_3_0().c_str(),
			   red , black , 0.5 , 1.5*font_size);

	rgbImg->draw_text( 0.05*Win_Width , 0.35*Win_Height,
			   MES_preferences.Lang->info_3_1().c_str(),
			   red , black , 0.6 , font_size);


      rgbImg->draw_text( 0.05*Win_Width , 0.82*Win_Height,
			 MES_preferences.Lang->info_3_2().c_str(),
			 red , black , 0.5 , font_size );

    sky_disp->display( (*rgbImg) ) ;
  }
 
  void info_get_image()
  {
    rgbImg->draw_text( 0.5*Win_Width , 0.5*Win_Height,
		       MES_preferences.Lang->info_get_image().c_str(),
		       red , black , 0.8 , font_size );
    sky_disp->display( (*rgbImg) ) ;
  }

  void info_get_stars()
  {
    rgbImg->draw_text( 0.5*Win_Width , 0.55*Win_Height,
		       MES_preferences.Lang->info_get_stars().c_str(),
		       red , black , 0.8 , font_size );
    sky_disp->display( (*rgbImg) ) ;
  }

 void info_get_center()
  {
    rgbImg->draw_text( 0.5*Win_Width , 0.6*Win_Height,
		       MES_preferences.Lang->info_get_center().c_str(),
		       red , black , 0.8 , font_size );
    sky_disp->display( (*rgbImg) ) ;
  }

 void info4( string s, int nb) // print message 
  {
    stringstream s1;
    s1 << s << nb ;
    rgbImg->draw_text( 0.25*Win_Width , 0.5*Win_Height,
		       s1.str().c_str(),
		       red , black , 0.8 , 3*font_size );
    sky_disp->display( (*rgbImg) ) ;
  }
  
 void info4( string s) // print message after 
  {
    rgbImg->draw_text( 0.25*Win_Width , 0.5*Win_Height,
		       s.c_str(),
		       red , black , 0.8 , 3*font_size );
    sky_disp->display( (*rgbImg) ) ;
  }
  
  void draw_img( CImg<float>* img)
  {
    CImg<unsigned char> local = (*img);
    if((local.width() != Win_Width) || (local.height() != Win_Height ))
      local.resize(Win_Width,Win_Height);
    local.move_to( (*rgbImg) ); 
    sky_disp->display( (*rgbImg) ) ;
  }
  void draw_img( CImg<unsigned char>* img)
  {
    if((img->width() != Win_Width) || ( img->height() != Win_Height ))
      {
	CImg<unsigned char> local(Win_Width, Win_Height, 1, 3);;
	local = img->get_resize(Win_Width,Win_Height,1 ,3);
	local.move_to( (*rgbImg) ); 
      }else {
      img->move_to( (*rgbImg) ); 
    }
   sky_disp->display( (*rgbImg) ) ;
  }
  
};

MES_DisplayT *MES_Display;
