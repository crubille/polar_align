/**
 * Star recognizer using the CImg library and CCFits.
 *
 * Copyright (C) 2020 Paul Crubill� Sorbonne universit�s, Universit� de Technologie de Compi�gne, CNRS, Heudiasyc UMR 7253, CS 60 319, 60203 Compi�gne Cedex France
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using namespace std;
#include <time.h>
#include <string>
#include <iostream>
#include <string>
#include <stdlib.h>
#include "lang.h"
#include "../getpot-c++/GetPot.hpp"
#include "preferences.h"

MES_preferencesT MES_preferences;

string Lang_DisplayT::help()
    {
      return "Help" ;
    }

string Lang_DisplayT::window_title()
    {
      return "C polar align" ;
    }

string Lang_DisplayT::info_0()
  {
    stringstream s0;
    s0 <<  "Help to align an equatorial mount using 2 images of the polar region of the sky."
      << endl
      << "Between the 2 images, the RA axis of the mount should be rotated."
       <<endl
       <<  "Then the software overprint the nominal position of stars around the computed" << endl
      << "rotation center and periodically get new images from the camera. " <<endl
       << "Then use the alt/az adjustment off the mount to bring the stars to their nominal position "
      << endl << endl
       << "Software options can be set in a config file or on the command line.";
    return s0.str();
  }
    
string Lang_DisplayT::info_1()
  {
    return  "Step 1:";
  }
  string Lang_DisplayT::info_1_1()
  {
stringstream s1;
    s1 <<  "1-1 - Align your mount to the pole using a compass for example" <<endl
       <<  "and adjust it for the lattitude.";
    return s1.str();
  }
  string Lang_DisplayT::info_1_2()
  {
    stringstream s2;
    s2 << "1-2 - Using the DEC adjustment set the camera to point to the polar region"
       << endl << 		       "(do not modify the DEC adjustment for the rest of the process).";
    return s2.str();
  }
  string Lang_DisplayT::info_1_3()
  {
    stringstream s3;
    s3 << "1-3 - Set the RA adjustment so that the camera image is tilted between 30 and 90 degrees" << endl
       <<  "(check the lens focus is good enough to see stars)";
    return s3.str();
  }
  string Lang_DisplayT::info_1_4()
  {
    stringstream st;
    st << "1-4 press \"y\", \"c\", <esc>, <space>, <cr> to process: get an image from the camera and go to step2," << endl 
       << "\"r\" to restart, \"q\" to quit.";
    return st.str();
  }
  string Lang_DisplayT::info_1_status()
  {
    stringstream ss;
    if( MES_preferences.Pole == NorthPole ) ss << "North pole" << endl;
      else ss << "South pole" << endl;
    if( MES_preferences.Lens == PhotoLens ) ss << "camera lens" << endl;
      else ss << "astro lens" << endl;
    ss << "longitude = " << MES_preferences.longitude << endl
      << "images prefix = " << MES_preferences.JpegName << endl
      << "pixel size = " << MES_preferences.PixelSize << endl
      << "focale = " << MES_preferences.Focale << endl ;
    if(MES_preferences.ra_motor_on) ss << "RA motor on";
    else ss << "ra motor off"; 
    return ss.str();
  }
  
  string Lang_DisplayT::info2() // print message after i get and display the first image
  {
    stringstream s1;
    s1 <<  "If you are not ok with this image" << endl
      << "check focus, iso and speed and" << endl
       << "type \"r\" to get another one.";
    return s1.str();
  }
  string Lang_DisplayT::info_2_0()
  {
    return "Step 2:";
  }
  string Lang_DisplayT::info_2_1()
  {
    stringstream s2;
    s2 << "2-1 Else in order to continue" << endl
       << "set the RA adjustment " << endl
       << "so that the camera image " <<endl
       << "is close to horizontal.";
    return s2.str();
  }
  string Lang_DisplayT::info_2_2()
  {
    stringstream st;
    st << "2-2  press \"y\", \"c\", <esc>, <space>, <cr> to process" << endl
       << "\"r\" to restart, \"q\" to quit, "
      ;
    return st.str();
  }

  string Lang_DisplayT::info_3() // print message after i get and display the first image
  {
    return "C Polar Align";
  }
  string Lang_DisplayT::info_3_00()
  {
    return "Rotation center detected.";
  }
  string Lang_DisplayT::info_3_0()
  {
    return "Step 3:";
  }
  string Lang_DisplayT::info_3_1()
  {
    stringstream s1;
    s1    << "Now, use the alt/az ajustment to put each" << endl
	  << "star at its position (in the yellow circle)."
	  << endl << "If needed, you can rotate the sight (to"
	  << endl << "adapt to imprecise camera position,"
	  << endl << "longitude or time synchronisation).";

    return s1.str();
  }
  string Lang_DisplayT::info_3_2()
  {
    stringstream st;
    st << "3-2  press \"y\", \"c\", <esc>, <space>, <cr> to get a new image" << endl
       << "\"r\" to restart, \"q\" to quit, " << endl
       << "\"z\", <left clic> to zoom on the mouse pointer position;" <<endl
       << "\"u\", <right clic> to unzoom (<esc> unzoom too)."<<endl
       << "\"+\"/\"-\" (from the numeric pad) to rotate stars position.";
    return st.str();
  }
 
  string Lang_DisplayT::info_get_image()
  {
    return "asking camera for an image";
  }

  string Lang_DisplayT::info_get_stars()
  {
    return "computing stars";
  }

 string Lang_DisplayT::info_get_center()
  {
    return "computing rotation center";
  }

 string  Lang_DisplayT::msg_info4_few_stars() // print message 
  {
    return " stars found. Centre detection will not be very good";
  }

 string Lang_DisplayT::msg_info4_unable() // print message 
  {
    return " Unable to load image " ;
  }

 string Lang_DisplayT::msg_info4_no_center() // print message 
  {
    return  "Rotation center not found";
  }

 string Lang_DisplayT::msg_info4_no_star() // print message 
  {
    return  "Not enough stars found " ;
  }


Lang_DisplayT En;


///////////////////////////////////////////////////////////////////////////
//           Francais
///////////////////////////////////////////////////////////////////////////
class Fr_DisplayT : public Lang_DisplayT
{
 public:
  string help()
    {
      return "Help" ;
    }

  string windows_title()
    {
      return "C polar align" ;
    }

  string info_0()
  {
    stringstream s0;
    s0 <<  "Aide � l'aligmement d'une monture �quatoriale en utilisant 2 images proche du p�le c�leste."
      << endl
      << "Entre les 2 images, il faut faire pivoter la monture en ascencion droite."
       <<endl
       <<  "Le logiciel indique alors la position nominale des �toiles du champ par rapport au centre" << endl
      << "de rotation calcul� puis permet l'acquisition de nouvelles images depuis l'appareil photo. " <<endl
       << "Il suffit alors de faire coincider les �toiles avec leur position nominale."
      << endl << endl
       << "Le logiciel peut �tre configur� par diverses options en ligne de commande ou dans le fichier \"config.cpa\" .";
    return s0.str();
  }
    
  string info_1()
  {
    return  "Etape 1:";
  }
  string info_1_1()
  {
stringstream s1;
    s1 <<  "1-1 - Diriger l'axe de la monture vers le p�le (par exemple � la boussole)" <<endl
       <<  "et la monture �tant de niveau, r�gler la lattitude.";
    return s1.str();
  }
  string info_1_2()
  {
    stringstream s2;
    s2 << "1-2 - Pointer la r�gion polaire avec l'appareil photo "
       << endl <<  "(pour tout le reste de la proc�dure, ne modifier plus la d�clinaison).";
    return s2.str();
  }
  string info_1_3()
  {
    stringstream s3;
    s3 << "1-3 - Pivoter la monture selon l'axe AD pour que l'image soit inclin�e entre 30 et 90 degr�s" << endl
       <<  "(v�rifier que le temps de pose, la sensibilit� et la mise au point  permettent de voir les �toiles)";
    return s3.str();
  }
  string info_1_4()
  {
    stringstream st;
    st << "1-4 appuyer sur les touches \"y\", \"c\", <esc>, <space>, <cr> pour lancer la proc�dure: l'appareil prend la premi�re photo," << endl 
       << "\"r\" retour au d�but, \"q\" pour quitter.";
    return st.str();
  }
  string info_1_status()
  {
    stringstream ss;
    if( MES_preferences.Pole == NorthPole ) ss << "P�le nord" << endl;
      else ss << "P�le sud" << endl;
    if( MES_preferences.Lens == PhotoLens ) ss << "objectif photo" << endl;
      else ss << "instrument astro" << endl;
    ss << "longitude = " << MES_preferences.longitude << endl
      << "pr�fix d'image = " << MES_preferences.JpegName << endl
      << "taille d'un pixel = " << MES_preferences.PixelSize << endl
      << "focale = " << MES_preferences.Focale << endl ;
    if(MES_preferences.ra_motor_on) ss << "suivi actif";
    else ss << "motorisation inactive"; 
    return ss.str();
  }
  
  string info2() // print message after i get and display the first image
  {
    stringstream s1;
    s1 <<  "Si ce premier clich� n'est pas satisfaisant," << endl
      << "v�rifier la mise au point, la vitesse et les iso et" << endl
       << "appuyer sur \"r\" pour recommencer au d�but.";
    return s1.str();
  }
  string info_2_0()
  {
    return "Etape 2:";
  }
  string info_2_1()
  {
    stringstream s2;
    s2 << "2-1 Sinon, pour continuer" << endl
       << "pivoter la monture selon l'axe AD " << endl
       << "afin que l'image de l'appareil photo" <<endl
       << "soit proche de l'horizontale.";
    return s2.str();
  }
  string info_2_2()
  {
    stringstream st;
    st << "2-2  touches \"y\", \"c\", <esc>, <space>, <cr> pour continuer la proc�dure" << endl
       << "\"r\" recommencer au d�but, \"q\" quitter, "
      ;
    return st.str();
  }

  string info_3() // print message after i get and display the first image
  {
    return "C Polar Align";
  }
  string info_3_00()
  {
    return "Centre de rotation trouv�.";
  }
  string info_3_0()
  {
    return "Etape 3:";
  }
  string info_3_1()
  {
    stringstream s1;
    s1    << "Maintenant aligner la monture avec les r�glage alt/az" << endl
	  << "pour mettre chaque �toile � sa place (dans le petit cercle jaune)."
	  << endl << "Si l'appareil n'est pas bien orient�, la longitude n'est pas correcte ou l'heure pas r�gl�e,"
	  << endl << "vous pouvez tourner la monture en AD ou plus simplement faire tourner la mire."
      ;

    return s1.str();
  }
  string info_3_2()
  {
    stringstream st;
    st << "3-2  appuyez sur \"y\", \"c\", <esc>, <space>, <cr> pour prendre une nouvelle photo" << endl
       << "\"r\" recommencer au d�but, \"q\" pour quitter, " << endl
       << "\"z\", <left clic> pour zoomer (� 100%) � la position de la souris;" <<endl
       << "\"u\", <right clic> pour d�zoomer (<esc> d�zoomer et prendre une nouvelle photo)."<<endl
       << "\"+\"/\"-\" (sur le pav� num�rique) pour faire tourner la mire des positions des �toiles.";
    return st.str();
  }
 
  string info_get_image()
  {
    return "capture d'image en cours";
  }

  string info_get_stars()
  {
    return "localisation des �toiles";
  }

 string info_get_center()
  {
    return "calcul du centre de rotation";
  }

 string  msg_info4_few_stars() // print message 
  {
    return " �toiles trouv�es. calcul peu pr�cis.";
  }

 string msg_info4_unable() // print message 
  {
    return " Impossible de charger l'image!" ;
  }

  virtual string msg_info4_no_center() // print message 
  {
    return  "Centre de rotation non determine!";
  }

 string msg_info4_no_star() // print message 
  {
    return  " Pas assez d'�toiles d�tect�es!" ;
  }
};

Lang_DisplayT *Fr = new Fr_DisplayT;

////////////////////////////////////////////////////////////////

class Short_text_DisplayT : public Lang_DisplayT
{
string info_0()
  {
    stringstream s0;
    s0 <<  "Help to align an equatorial mount"
       << endl << "using 2 images of the polar region of the sky."
       << endl << "Between the 2 images, the RA axis should be rotated."
       << endl << "Use the alt/az adjustment to bring the stars to their position "
       << endl << endl
       << "Options in a config file or on the command line.";
    return s0.str();
  }

  string info_1_status()
  {
    stringstream ss;
    if( MES_preferences.Pole == NorthPole ) ss << "North pole" << endl;
      else ss << "South pole" << endl;
    if( MES_preferences.Lens == PhotoLens ) ss << "camera lens" << endl;
      else ss << "astro lens" << endl;
    ss << "longitude = " << MES_preferences.longitude << endl
      << "pixel size = " << MES_preferences.PixelSize << endl
      << "focale = " << MES_preferences.Focale << endl ;
    if(MES_preferences.ra_motor_on) ss << "RA motor on";
    else ss << "ra motor off"; 
    return ss.str();
  }
  

  string info_1_1()
  {
stringstream s1;
 s1 <<  "1-1 - Direct your mount towards the pole" ;
 return s1.str();
  }
  string info_1_2()
  {
    stringstream s2;
    s2 << "1-2 - set the camera to point to the polar region";
    return s2.str();
  }
  string info_1_3()
  {
    stringstream s3;
    s3 << "1-3 - Tilted the RA adjustment between 30 and 90 degrees";
    return s3.str();
  }
  string info_1_4()
  {
    stringstream st;
    st << "1-4 press \"y\", \"c\", <esc>, <space>, <cr> to get the image," << endl 
       << "\"r\" to restart, \"q\" to quit.";
    return st.str();
  }

  string info_3_1()
  {
    stringstream s1;
    s1    << "Use the alt/az ajustment to put each" << endl
	  << "star in its yellow circle."
	  << endl << "If needed, you can rotate the sight";

    return s1.str();
  }
  string info_3_2()
  {
    stringstream st;
    st << "3-2  press \"y\", \"c\", <esc>, <space>, <cr> new image" << endl
       << "\"r\" restart, \"q\" quit, " << endl
       << "\"z\", <left clic> zoom; \"u\", <right clic> unzoom."<<endl
       << "\"+\"/\"-\" (from the numeric pad) rotate the sight.";
    return st.str();
  }
 
  
};
 
Lang_DisplayT *Short_Text = new Short_text_DisplayT;

